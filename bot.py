# Work with Python 3.6
import discord
import json
import os
import time
import re
import requests




client = discord.Client()

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('!hello'):
        msg = 'Hello {0.author.mention}'.format(message)
        await client.send_message(message.channel, msg)

    if message.content.startswith('!armory'):
        info = message.content.split()
        response = requests.get("https://raider.io/api/v1/characters/profile?region=eu&realm={0}&name={1}&fields=gear%2Cguild%2Craid_progression%2Cmythic_plus_scores%2Cmythic_plus_best_runs".format(info[1],info[2]))
        data = response.json()
        embed = discord.Embed(title='Character info', url="https://raider.io/characters/eu/{0}/{1}".format(info[1],info[2]))
        embed.set_thumbnail(url=data['thumbnail_url'])
        embed.add_field(name='Name', value=data['name'])
        embed.add_field(name='Active Spec', value=data['active_spec_name'])
        embed.add_field(name='Achievment Points', value=data['achievement_points'])
        embed.add_field(name='Mythic Plus score', value=data['mythic_plus_scores']['all'])
        embed.add_field(name="iLvl", value=data['gear']['item_level_equipped'])
        await client.send_message(message.channel, embed=embed)

    if message.content.startswith('!affix'):
        raiderio_affix = requests.get("https://raider.io/api/v1/mythic-plus/affixes?region=eu&locale=en")
        affix = raiderio_affix.json()
        embed = discord.Embed(title='Mythic +2 level')
        embed.add_field(name='Affix', value=affix['affix_details'][0]['name'])
        embed.add_field(name='Description', value=affix['affix_details'][0]['description'])
        embed = discord.Embed(title='Mythic +4 level')
        embed.add_field(name='Affix', value=affix['affix_details'][1]['name'])
        embed.add_field(name='Description', value=affix['affix_details'][1]['description'])
        await client.send_message(message.channel, embed=embed)

    if message.content.startswith('!gear'):
        gear_api = requests.get('https://eu.api.blizzard.com/wow/character/burning-legion/Kolaj?fields=items&locale=en_US&access_token=USuX3YARzRGdUs8ODpH5xySjxQ8TvwLOh1')
        gear = gear_api.json()
        embed = discord.Embed(title='items')
        embed.add_field(name='Head', value=gear['items']['head']['name'])
        embed.set_image(url='https://render-eu.worldofwarcraft.com/icons/56/%s' % gear['items']['head']['icon'] + '.jpg')
        embed.add_field(name='Neck', value=gear['items']['neck']['name'])
        embed.set_image(url='https://render-eu.worldofwarcraft.com/icons/56/%s' % gear['items']['neck']['icon'] + '.jpg')
        await client.send_message(message.channel, embed=embed)


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run('NDk4NDgwMTIyODYxNDUzMzM0.DugBOg.AB28mLsOJxtfuTFm0JPq13qbR-o')